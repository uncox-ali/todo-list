import {
    Checkbox,
    IconButton,
    List,
    ListItem,
    ListItemIcon,
    ListItemSecondaryAction,
    ListItemText,
} from '@material-ui/core';
import { makeStyles, Theme } from '@material-ui/core/styles';
import { Delete as DeleteIcon } from '@material-ui/icons';
import React from 'react';
import { DragDropContext, Draggable, Droppable, DropResult } from 'react-beautiful-dnd';

interface Task {
    id: string;
    title: string;
    description: string;
    completed: boolean;
}

interface Props {
    tasks: Task[] | [];
    filter: string;
    handleToggleComplete: (val: string) => void;
    handleEditTask: (val: string) => void;
    handleDeleteTask: (val: string) => void;
    handleDragEnd: (val: DropResult) => void;
}

const useStyles = makeStyles((theme: Theme) => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
        maxWidth: 300,
    },
    button: {
        margin: theme.spacing(1),
    },
    list: {
        marginTop: theme.spacing(2),
    },
}));

export default function TaskList({
    tasks,
    filter,
    handleToggleComplete,
    handleEditTask,
    handleDeleteTask,
    handleDragEnd,
}: Props) {
    const classes = useStyles();

    const filteredTasks = tasks.filter((task) => {
        if (filter === 'completed') {
            return task.completed;
        } else if (filter === 'notCompleted') {
            return !task.completed;
        } else {
            return true;
        }
    });

    return (
        <DragDropContext onDragEnd={handleDragEnd}>
            <Droppable droppableId="task-list">
                {(provided, snapshot) => (
                    <List
                        className={classes.list}
                        {...provided.droppableProps}
                        ref={provided.innerRef}
                        style={{
                            backgroundColor: snapshot.isDraggingOver ? '#eee' : 'inherit',
                        }}
                    >
                        {filteredTasks.map((task, index) => (
                            <Draggable key={task.id} draggableId={task.id.toString()} index={index}>
                                {(provided, snapshot) => (
                                    <ListItem
                                        className={`${
                                            index % 2 === 0 ? 'bg-neutral-200' : 'bg-white'
                                        }`}
                                        ref={provided.innerRef}
                                        {...provided.draggableProps}
                                        {...provided.dragHandleProps}
                                        style={{
                                            ...provided.draggableProps.style,
                                            backgroundColor: snapshot.isDragging
                                                ? '#ccc'
                                                : 'inherit',
                                        }}
                                    >
                                        <ListItemIcon>
                                            <Checkbox
                                                checked={task.completed}
                                                onClick={() =>
                                                    handleToggleComplete(task.id.toString())
                                                }
                                            />
                                        </ListItemIcon>
                                        <ListItemText
                                            primary={task.title}
                                            secondary={task.description}
                                        />
                                        <ListItemSecondaryAction>
                                            {/* <IconButton
                                            aria-label="edit"
                                            onClick={() => handleEditTask(task.id.toString())}
                                        >
                                            <EditIcon />
                                        </IconButton> */}
                                            <IconButton
                                                aria-label="delete"
                                                onClick={() => handleDeleteTask(task.id.toString())}
                                            >
                                                <DeleteIcon />
                                            </IconButton>
                                        </ListItemSecondaryAction>
                                    </ListItem>
                                )}
                            </Draggable>
                        ))}
                        {provided.placeholder}
                    </List>
                )}
            </Droppable>
        </DragDropContext>
    );
}
