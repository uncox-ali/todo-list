import { Button, FormControl, InputLabel, MenuItem, Select, TextField } from '@material-ui/core';
import { makeStyles, Theme } from '@material-ui/core/styles';
import { Add as AddIcon, Delete as DeleteIcon } from '@material-ui/icons';
import { Box } from '@mui/material';
import React, { useState } from 'react';
import { DropResult } from 'react-beautiful-dnd';

import TaskList from './TaskList';

interface Task {
    id: string;
    title: string;
    description: string;
    completed: boolean;
}

interface State {
    tasks: Task[];
    title: string;
    description: string;
    filter: 'all' | 'completed' | 'notCompleted';
}

const useStyles = makeStyles((theme: Theme) => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
        maxWidth: 300,
    },
    button: {
        margin: theme.spacing(1),
    },
    list: {
        marginTop: theme.spacing(2),
    },
}));

function App() {
    const classes = useStyles();
    const [state, setState] = useState<State>({
        tasks: [],
        title: '',
        description: '',
        filter: 'all',
    });

    const handleAddTask = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        if (state.title.trim()) {
            const newTask: Task = {
                id: Date.now().toString(),
                title: state.title,
                description: state.description,
                completed: false,
            };
            setState({
                ...state,
                tasks: [...state.tasks, newTask],
                title: '',
                description: '',
            });
        }
    };

    const handleDeleteTask = (id: string) => {
        setState({
            ...state,
            tasks: state.tasks.filter((task) => task.id !== id),
        });
    };

    const handleEditTask = (id: string) => {
        const taskIndex = state.tasks.findIndex((task) => task.id === id);
        if (taskIndex !== -1) {
            const editedTask: Task = {
                ...state.tasks[taskIndex],
                title: state.title,
                description: state.description,
            };
            const newTasks = [...state.tasks];
            newTasks[taskIndex] = editedTask;
            setState({
                ...state,
                tasks: newTasks,
                title: '',
                description: '',
            });
        }
    };

    const handleToggleComplete = (id: string) => {
        const taskIndex = state.tasks.findIndex((task) => task.id === id);
        if (taskIndex !== -1) {
            const newTasks = [...state.tasks];
            newTasks[taskIndex].completed = !newTasks[taskIndex].completed;
            setState({
                ...state,
                tasks: newTasks,
            });
        }
    };

    const handleFilterChange = (event: React.ChangeEvent<{ value: unknown }>) => {
        setState({
            ...state,
            filter: event.target.value as State['filter'],
        });
    };

    const handleDragEnd = (result: DropResult) => {
        if (!result.destination) {
            return;
        }

        const newTasks = Array.from(state.tasks);
        const [removed] = newTasks.splice(result.source.index, 1);
        newTasks.splice(result.destination.index, 0, removed);
        setState({
            ...state,
            tasks: newTasks,
        });
    };

    return (
        <Box className="w-full flex justify-center">
            <Box className="w-full container">
                <form onSubmit={handleAddTask}>
                    <Box className=" flex justify-between gap-10 items-center">
                        <TextField
                            label="Title"
                            value={state.title}
                            onChange={(event) => setState({ ...state, title: event.target.value })}
                            fullWidth
                        />
                        <TextField
                            label="Description"
                            value={state.description}
                            onChange={(event) =>
                                setState({ ...state, description: event.target.value })
                            }
                            fullWidth
                        />
                        <FormControl className={classes.formControl} fullWidth>
                            <InputLabel id="filter-label">Filter by Completion Status</InputLabel>
                            <Select
                                labelId="filter-label"
                                value={state.filter}
                                onChange={handleFilterChange}
                            >
                                <MenuItem value="all">All</MenuItem>
                                <MenuItem value="completed">Completed</MenuItem>
                                <MenuItem value="notCompleted">Not Completed</MenuItem>
                            </Select>
                        </FormControl>
                    </Box>
                    <Button
                        variant="contained"
                        color="primary"
                        className={classes.button}
                        startIcon={<AddIcon />}
                        type="submit"
                    >
                        Add Task
                    </Button>
                    <Button
                        variant="contained"
                        color="secondary"
                        className={classes.button}
                        startIcon={<DeleteIcon />}
                        onClick={() => setState({ ...state, tasks: [] })}
                    >
                        Clear All
                    </Button>
                </form>

                <TaskList
                    tasks={state.tasks}
                    filter={state.filter}
                    handleToggleComplete={handleToggleComplete}
                    handleDeleteTask={handleDeleteTask}
                    handleEditTask={handleEditTask}
                    handleDragEnd={handleDragEnd}
                />
            </Box>
        </Box>
    );
}

export default App;
